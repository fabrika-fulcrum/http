<?php

namespace Fulcrum\Http;

class Request
{
    public const GET = 'GET';
    public const POST = 'POST';
    public const PUT = 'PUT';
    public const PATCH = 'PATCH';
    public const DELETE = 'DELETE';

    /**
     * @var RequestItem
     */
    protected static $current = null;

    public static function getCurrent()
    {
        if (static::$current === null) {
            static::$current = RequestItem::fromGlobals();
        }
        return static::$current;
    }

    public static function file($i, $j = null)
    {
        return static::getCurrent()->file($i, $j);
    }

    public static function files()
    {
        return static::getCurrent()->files();
    }

    public static function path()
    {
        return static::getCurrent()->path();
    }

    public static function segment($index)
    {
        return static::getCurrent()->segment($index);
    }

    public static function segments()
    {
        return static::getCurrent()->segments();
    }

    public static function setData($data)
    {
        return static::getCurrent()->setData($data);
    }

    public static function data($key = null)
    {
        return static::getCurrent()->data($key = null);
    }

    public static function hasData($key)
    {
        return static::getCurrent()->hasData($key);
    }

    public static function hasParameter($key)
    {
        return static::getCurrent()->hasParameter($key);
    }

    public static function parameter($key)
    {
        return static::getCurrent()->parameter($key);
    }

    public static function parameters()
    {
        return static::getCurrent()->parameters();
    }

    public static function method()
    {
        return static::getCurrent()->method();
    }

    public static function isGet()
    {
        return static::getCurrent()->isGet();
    }

    public static function isPost()
    {
        return static::getCurrent()->isPost();
    }

    public static function isPut()
    {
        return static::getCurrent()->isPut();
    }

    public static function isPatch()
    {
        return static::getCurrent()->isPatch();
    }

    public static function isDelete()
    {
        return static::getCurrent()->isDelete();
    }
}

