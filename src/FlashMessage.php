<?php

namespace Fulcrum\Http;

class FlashMessage
{
    const FLASH_INFO = 'info';
    const FLASH_SUCCESS = 'success';
    const FLASH_WARNING = 'warning';
    const FLASH_ERROR = 'error';

    protected $message;
    protected $status;

    public function __construct($message, $status = null)
    {
        if (is_null($status)) {
            $status = self::FLASH_INFO;
        }
        if (is_a($message, static::class)) {
            $status = $message->getStatus();
            $message = $message->getMessage();
        }
        $this->status = $status;
        $this->message = $message;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function getStatus()
    {
        return $this->status;
    }

}
