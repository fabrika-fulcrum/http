<?php

namespace Fulcrum\Http;

class AjaxResponse implements \JsonSerializable
{
    public const ERROR_PARSE_ERROR = -32700;
    public const ERROR_INVALID_REQUEST = -32600;
    public const ERROR_METHOD_NOT_FOUND = -32601;
    public const ERROR_INVALID_PARAMS = -32602;
    public const ERROR_INTERNAL_ERROR = -32603;

    protected $data;
    protected $errorMessage;
    protected $errorCode;
    protected $errors = [];

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @param mixed $message
     * @param int|null $code
     */
    public function setError($message, $code = null)
    {
        if ($code === null) {
            $code = static::ERROR_INTERNAL_ERROR;
        }
        $this->errorMessage = $message;
        $this->errorCode = $code;
    }

    /**
     * @param string $key
     * @param string $message
     */
    public function addError($key, $message)
    {
        $this->errors[$key] = $message;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        $result = [];
        if ($this->errorCode !== null) {
            $result['error'] = [
                'code' => $this->errorCode,
                'message' => $this->errorMessage,
                'data' => $this->errors
            ];
        } else {
            if ($this->data !== null) {
                $result['result'] = $this->data;
            } else {
                $result['result'] = true;
            }
        }
        return $result;
    }
}
