<?php

namespace Fulcrum\Http;

class SessionInstance
{
    protected const BAG_KEY = '_session_bags';
    protected const FLASH_BAG_KEY = '_session_flash_bags';
    protected $bags = [];
    protected $flashbag = null;

    public function __construct()
    {
        $this->loadBags();
    }

    protected function loadBags()
    {
        $this->bags = [];
        if (!isset($_SESSION[self::BAG_KEY])) {
            $_SESSION[self::BAG_KEY] = [];
        }
        $this->bags = $_SESSION[self::BAG_KEY];

        $this->flashbag = null;
        if (!isset($_SESSION[self::FLASH_BAG_KEY])) {
            $_SESSION[self::FLASH_BAG_KEY] = new SessionFlashBag();
        }
        $this->flashbag = $_SESSION[self::FLASH_BAG_KEY];
    }

    /**
     * @return SessionFlashBag
     */
    public function getFlashBag()
    {
        return $this->flashbag;
    }

    /**
     * @param $name
     * @return SessionBag
     */
    public function getBag($name)
    {
        if (!array_key_exists($name, $this->bags)) {
            $newBag = new SessionBag($name, []);
            $this->bags[$name] = $newBag;
            $_SESSION[self::BAG_KEY] = $this->bags;
        }
        return $this->bags[$name];
    }

    public function has($key)
    {
        return isset($_SESSION[$key]);
    }

    public function get($key)
    {
        if (self::has($key)) {
            return $_SESSION[$key];
        } else {
            return null;
        }
    }

    public function set($key, $value)
    {
        if ($value === null) {
            unset($_SESSION[$key]);
        } else {
            $_SESSION[$key] = $value;
        }
    }

    public function clear()
    {
        session_destroy();
        session_start();
        $this->loadBags();
    }


}
