<?php

namespace Fulcrum\Http;

use Fulcrum\Http\Exception\InvalidHttpStatusException;

class Response
{
    const REDIRECT_PERM = 301;
    const REDIRECT_FOUND = 302;
    const REDIRECT_SEE_OTHER = 303;
    const REDIRECT_TEMP = 307;
    const REDIRECT_PERM_NOT_MODIFIED = 308;

    const HEADER_CONTENT_TYPE = 'Content-type';
    const HEADER_CONTENT_LENGTH = 'Content-length';
    const HEADER_STATUS = 'Status';

    const STATUS_200_OK = '200 OK';

    const STATUS_301_MOVED_PERMANENTLY = '301 Moved Permanently';
    const STATUS_302_FOUND = '302 Found';
    const STATUS_303_SEE_OTHER = '303 See Other';
    const STATUS_307_TEMPORARY_REDIRECT = '307 Temporary Redirect';
    const STATUS_308_NOT_MODIFIED = '308 Not Modified';

    const STATUS_400_BAD_REQUEST = '400 Bad Request';
    const STATUS_401_UNAUTHORIZED = '401 Unauthorized';
    const STATUS_403_FORBIDDEN = '403 Forbidden';
    const STATUS_404_NOT_FOUND = '404 Not Found';

    const STATUS_500_INTERNAL_SERVER_ERROR = '500 Internal Server Error';

    protected $headers = [];
    protected $status = '';

    protected $contents = '';

    public function __construct()
    {
        $this->status = static::STATUS_200_OK;
    }

    public function setStatus($status)
    {
        if (!in_array($status, [
            static::STATUS_200_OK,
            static::STATUS_301_MOVED_PERMANENTLY,
            static::STATUS_302_FOUND,
            static::STATUS_303_SEE_OTHER,
            static::STATUS_307_TEMPORARY_REDIRECT,
            static::STATUS_400_BAD_REQUEST,
            static::STATUS_401_UNAUTHORIZED,
            static::STATUS_403_FORBIDDEN,
            static::STATUS_404_NOT_FOUND,
            static::STATUS_500_INTERNAL_SERVER_ERROR
        ])) {
            throw new InvalidHttpStatusException(sprintf('Invalid http status: "%s"', $status));
        }
        $this->status = $status;
    }

    public function setHeader($param, $value)
    {
        $this->headers[$param] = $value;
    }


    protected function outputHeaders()
    {
        foreach ($this->headers as $key => $value) {
            header($key . ': ' . $value);
        }
    }

    public function output($contents = null)
    {
        $this->outputHeaders();
        echo $contents ?? $this->contents;
        die();
    }

    public function downloadFile(string $path, ?string $filenamme = null, ?string $mimeType = null)
    {
        $this->setHeader(static::HEADER_CONTENT_LENGTH, filesize($path));
        $this->setHeader("Cache-Control", 'public');
        $this->setHeader("Content-Description", "File Transfer");
        $this->setHeader("Content-Disposition", 'attachment; filename=' . $filenamme ?? pathinfo($path)['basename']);
        $this->setHeader(static::HEADER_CONTENT_TYPE, $mimeType ?? Mimes::getType(pathinfo($path)['extension']));
        $this->setHeader("content-Transfer-Encoding", "binary");
        $this->outputHeaders();
        readfile($path);
        exit;
    }

    public function outputFile(string $path, ?string $filename = null)
    {
        $this->setHeader(static::HEADER_CONTENT_LENGTH, filesize($path));
        $this->setHeader("Content-Description", "File Transfer");
        $this->setHeader(static::HEADER_CONTENT_TYPE, Mimes::getType(pathinfo($path)['extension']));
        $this->outputHeaders();
        readfile($path);
        exit;
    }

    public function outputAsFile(string $content, string $name, ?string $mime = null)
    {

    }

    public function redirect($to, $code = self::STATUS_302_FOUND)
    {
        http_response_code($code);
        header('Location: ' . $to);
        die();
    }

    public function setContents($contents)
    {
        $this->contents = $contents;
    }
}
