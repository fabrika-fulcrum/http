<?php

namespace Fulcrum\Http;

use Fulcrum\Filesystem\Path;

class RequestFile
{
    protected $name;
    protected $type;
    protected $tmp_name;
    protected $error;
    protected $size;
    protected $extension;

    public static function fromFile($filename)
    {
        $result = null;
        if (file_exists($filename)) {
            $name = pathinfo($filename, PATHINFO_FILENAME);
            $extension = pathinfo($filename, PATHINFO_EXTENSION);
            $tmp_name = $filename;
            $size = filesize($filename);

            $result = new RequestFile($name, $extension, $tmp_name, UPLOAD_ERR_OK, $size);
        }
        return $result;
    }

    public static function fromGlobalFiles($files)
    {
        $result = [];
        foreach ($files as $key => $item) {
            $result[$key] = static::FromGlobalFileArrays($item);
        }
        return $result;
    }

    protected static function fromGlobalFileArrays($files)
    {
        if (is_string($files['name'])) {
            $nameParts = explode('.', $files['name']);
            if (count($nameParts) == 1) {
                $ext = '';
            } else {
                $ext = $nameParts[count($nameParts) - 1];
            }
            return new RequestFile($files['name'], $ext, $files['tmp_name'], $files['error'], $files['size']);
        } else if (is_array($files['name'])) {
            return static::rewireFilesArray($files['name'], $files['type'], $files['tmp_name'], $files['error'], $files['size']);
        }
    }

    protected static function rewireFilesArray($name, $type, $tmp_name, $error, $size)
    {
        $result = [];
        foreach ($name as $k => $v) {
            if (is_array($v)) {
                $result[$k] = self::rewireFilesArray($name[$k], $type[$k], $tmp_name[$k], $error[$k], $size[$k]);
            } else {
                $nameParts = explode('.', $name[$k]);
                if (count($nameParts) == 1) {
                    $ext = '';
                } else {
                    $ext = $nameParts[count($nameParts) - 1];
                }
                $result[$k] = new RequestFile($name[$k], $ext, $tmp_name[$k], $error[$k], $size[$k]);
            }
        }
        return $result;
    }


    public function __construct($name, $extension, $tmp_name, $error, $size)
    {
        $this->name = $name;
        $this->extension = strtolower($extension);
        $this->tmp_name = $tmp_name;
        $this->error = $error;
        $this->size = $size;
        $this->type = $this->detectType();
    }


    /**
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @return mixed
     */
    public function getTmpName()
    {
        return $this->tmp_name;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    public function getExtension()
    {
        return $this->extension;
    }

    public function detectType()
    {
        $result = false;
        if ($this->isOk()) {
            $regexp = '/^([a-z\-]+\/[a-z0-9\-\.\+]+)(;\s.+)?$/';

            if (function_exists('finfo_file')) {
                $finfo = finfo_open(FILEINFO_MIME);
                if (is_resource($finfo)) {
                    $mime = @finfo_file($finfo, $this->tmp_name);
                    finfo_close($finfo);
                    if (is_string($mime) && preg_match($regexp, $mime, $matches)) {
                        $file_type = $matches[1];
                        $result = $file_type;
                    }
                }
            }
        }
        return $result;
    }

    public function isOk()
    {
        return $this->error == UPLOAD_ERR_OK;
    }

    public function discard()
    {
        unlink($this->tmp_name);
    }

    public function copy($dir, $filename)
    {
        if (!is_uploaded_file($this->tmp_name)) {
            return false;
        }
        return move_uploaded_file($this->tmp_name, Path::CreateDir($dir)->append($filename));
    }
}
