<?php

namespace Fulcrum\Http;

class SessionFlashBag
{
    protected $messages = [];

    public function add($message, $status = null)
    {
        if (is_a($message, FlashMessage::class)) {
            $newMessage = $message;
        } else {
            $newMessage = new FlashMessage($message, $status);
        }
        $this->messages[] = $newMessage;
    }

    public function success($message, ...$args)
    {
        $message = call_user_func_array('sprintf', array_merge([$message], $args));
        $this->add(new FlashMessage($message, FlashMessage::FLASH_SUCCESS));
    }

    public function warn($message, ...$args)
    {
        $message = call_user_func_array('sprintf', array_merge([$message], $args));
        $this->add(new FlashMessage($message, FlashMessage::FLASH_WARNING));
    }

    public function info($message, ...$args)
    {
        $message = call_user_func_array('sprintf', array_merge([$message], $args));
        $this->add(new FlashMessage($message, FlashMessage::FLASH_INFO));
    }

    public function error($message, ...$args)
    {
        $message = call_user_func_array('sprintf', array_merge([$message], $args));
        $this->add(new FlashMessage($message, FlashMessage::FLASH_ERROR));
    }

    public function clear()
    {
        $this->messages = [];
    }

    public function getMessages()
    {
        $result = $this->messages;
        $this->clear();
        return $result;
    }
}
