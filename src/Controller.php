<?php

namespace Fulcrum\Http;

class Controller
{
    /**
     * @var RequestItem
     */
    protected $request;
    protected $response;

    protected $data = [];

    public function __construct(RequestItem $request)
    {
        $this->request = $request;
        $this->response = new Response();
    }


}
