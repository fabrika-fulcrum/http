<?php

namespace Fulcrum\Http;

class Session //Facade
{
    private static $instance = null;
    protected static $initialized = false;

    protected static function Init()
    {
        if (static::$initialized) {
            return;
        }

        if (session_id() == '') {
            session_start();
        }
        if (static::$instance === null) {
            static::$instance = new SessionInstance();
        }
    }

    /**
     * @return SessionInstance
     */
    public static function getInstance()
    {
        static::Init();
        return static::$instance;
    }

    /**
     * @return SessionFlashBag
     */
    public static function getFlashBag()
    {
        return static::getInstance()->getFlashBag();
    }


    /**
     * @param $name
     * @return SessionBag
     */
    public static function getBag($name)
    {
        return static::getInstance()->getBag($name);
    }

    public static function has($key)
    {
        return static::getInstance()->has($key);
    }

    public static function get($key)
    {
        return static::getInstance()->get($key);
    }

    public static function set($key, $value)
    {
        static::getInstance()->set($key, $value);
    }

    public static function clear()
    {
        static::getInstance()->clear();

    }


}
