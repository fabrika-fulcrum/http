<?php

namespace Fulcrum\Http;

class RequestItem
{
    const GET = 'GET';
    const POST = 'POST';
    const PUT = 'PUT';
    const PATCH = 'PATCH';
    const DELETE = 'DELETE';

    const PROTOCOL_HTTP = 'http';
    const PROTOCOL_HTTPS = 'https';

    protected $method = '';

    protected $protocol = '';
    protected $domain = '';

    public $path = '';
    public $segments = [];

    protected $parameters = [];

    protected $data = [];

    protected $files = [];

    protected $xhr = false;

    public static function fromGlobals()
    {
        $method = static::GET;
        if ($_SERVER['REQUEST_METHOD'] === static::POST) {
            if (isset($_POST['_method']) && in_array(strtoupper($_POST['_method']), [static::PATCH, static::PUT, static::DELETE])) {
                $method = strtoupper($_POST['_method']);
            } else {
                $method = static::POST;
            }
        }

        $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] ? static::PROTOCOL_HTTPS : static::PROTOCOL_HTTP;
        $domain = $_SERVER['HTTP_HOST'];
        $uri = $_SERVER['REQUEST_URI'];
        $parts = explode('?', $uri);
        $path = $parts[0];
        $parameters = $_GET;
        $data = $_GET;
        if ($_SERVER['REQUEST_METHOD'] === static::POST) {
            $data = $_POST;
        }
        $files = $_FILES;
        $xhr = isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest';
        return new static($method, $protocol, $domain, $path, $parameters, $data, $files, $xhr);
    }

    public function __construct($method = 'GET', $protocol = "http", $domain = '', $path = '', $parameters = [], $data = [], $files = [], $xhr = false)
    {
        $this->method = $method;
        $this->protocol = $protocol;
        $this->domain = $domain;
        $this->setPath($path);
        $this->parameters = $parameters;
        $this->setData($data);
        $this->prepareFiles($files);
        $this->xhr = $xhr;
    }

    protected function prepareFiles($files)
    {
        $this->files = RequestFile::FromGlobalFiles($files);
    }

    /**
     * @param $i
     * @param null $j
     * @return RequestFile[]
     */
    public function file($i, $j = null)
    {
        $result = null;
        if (is_null($j)) {
            $result = (isset($this->files[$i]) ? $this->files[$i] : false);
        } else {
            $result = (isset($this->files[$i][$j]) ? $this->files[$i][$j] : false);
        }
        return $result;
    }

    public function files()
    {
        return $this->files;
    }

    public function setPath($path)
    {
        $this->path = $path . ((strlen($path) > 0 && $path[-1] !== '/') ? '/' : '');
        $chunks = explode('/', $path);

        if (count($chunks) > 0 && $chunks[0] == '') {
            array_shift($chunks);
        }
        if (count($chunks) > 0 && $chunks[count($chunks) - 1] === '') {
            array_pop($chunks);
        }
        $this->segments = $chunks;
    }

    public function path()
    {
        return $this->path;
    }

    public function segment($index)
    {
        if (isset($this->segments[$index])) {
            return $this->segments[$index];
        }
        return '';
    }

    public function segments()
    {
        return $this->segments;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function data($key = null)
    {
        if (func_num_args() == 1) {
            return $this->data[$key];
        }
        return $this->data;
    }

    public function hasData($key)
    {
        return isset($this->data[$key]);
    }


    public function hasParameter($key)
    {
        return isset($this->parameters[$key]);
    }

    public function parameter($key)
    {
        return $this->parameters[$key];
    }

    public function parameters()
    {
        return $this->parameters;
    }

    public function method()
    {
        return $this->method;
    }

    public function protocol()
    {
        return $this->protocol;
    }

    public function domain()
    {
        return $this->domain;
    }

    public function isGet()
    {
        return $this->method == static::GET;
    }

    public function isPost()
    {
        return $this->method == static::POST;
    }

    public function isPut()
    {
        return $this->method == static::PUT;
    }

    public function isPatch()
    {
        return $this->method == static::PATCH;
    }

    public function isDelete()
    {
        return $this->method == static::DELETE;
    }

    public function isXHR()
    {
        return $this->xhr;
    }

}
