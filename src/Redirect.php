<?php

namespace Fulcrum\Http;

use Fulcrum\Router\Router;

class Redirect
{
    protected $url;
    protected $route;
    protected $routeName;
    protected $fullyQualified = false;
    protected $code;
    protected $params = [];
    protected $formData;

    public static function to(string $destination)
    {
        $result = new static();
        $result->setUrl($destination);
        return $result;
    }

    public static function path($routeName)
    {
        $result = new static();
        $result->setRouteName($routeName);
        return $result;
    }

    public function __construct()
    {
        $this->code = Response::REDIRECT_FOUND;
    }

    public function with($params)
    {
        if (\func_num_args() === 1 && is_array($params)) {
            $this->params = array_merge($this->params, $params);
        } else if (func_num_args() === 2) {
            $this->params[func_get_arg(0)] = func_get_arg(1);
        }
        return $this;
    }

    public function fullyQualified()
    {
        $this->fullyQualified = true;
        return $this;
    }

    private function setRouteName($routeName)
    {
        $this->routeName = $routeName;
    }

    private function setUrl($url)
    {
        $this->url = $url;
    }

    public function temporary($code = Response::REDIRECT_FOUND)
    {
        $this->code = $code;
        return $this;
    }

    public function permanent($code = Response::REDIRECT_PERM)
    {
        $this->code = $code;
        return $this;
    }

    public function withFormData($formData = [])
    {
        $this->formData = $formData;
        return $this;
    }

    public function go()
    {
        $response = new Response();
        if ($this->routeName) {
            $this->url = Router::path($this->routeName, $this->params);
        }
        if ($this->formData) {
            Session::set('formData', $this->formData);
        }
        $response->redirect($this->url, $this->code);
        die();
    }
}
