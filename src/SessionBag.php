<?php

namespace Fulcrum\Http;

class SessionBag
{
    protected $name = '';
    protected $contents = [];

    public function __construct($name, $values)
    {
        $this->name = $name;
        $this->contents = $values;
    }

    public function has($key)
    {
        return (isset($this->contents[$key]));
    }

    public function get($key)
    {
        if (isset($this->contents[$key])) {
            return $this->contents[$key];
        } else {
            return null;
        }
    }

    public function delete($key)
    {
        if (isset($this->contents[$key])) {
            unset($this->contents[$key]);
        }
    }

    public function set($key, $value)
    {
        $this->contents[$key] = $value;
    }

    public function all()
    {
        return $this->contents;
    }

    public function emptyAll()
    {
        $this->contents = [];
    }

    public function is_empty()
    {
        return count($this->contents) == 0;
    }
}
